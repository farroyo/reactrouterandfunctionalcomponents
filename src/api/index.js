import Controls from './controls.json';

const DELAY = 1500;

let getControlsAllPromise,
    getControlsImplementedPromise,
    getControlsNotImplementedPromise,
    getControlsUnknownPromise;


const getControlsAll = () => {
  if (getControlsAllPromise) {
    // A promise to getControls() is already in-flight.  Return it rather than
    // create a new promise
    return getControlsAllPromise;
  }

  getControlsAllPromise = new Promise(res => {
    setTimeout(() => {
      getControlsAllPromise = null;
      return res(Controls)
    }, DELAY)
  })

  return getControlsAllPromise;
}

const getControlsImplemented = () => {
  if (getControlsImplementedPromise) {
    // A promise to getControls() is already in-flight.  Return it rather than
    // create a new promise
    return getControlsImplementedPromise;
  }

  getControlsImplementedPromise = new Promise(res => {
    setTimeout(() => {
      getControlsImplementedPromise = null;
      
      return res(Controls.filter(c => c.state && c.state.isImplemented))
    }, DELAY)
  })

  return getControlsImplementedPromise;
}
const getControlsNotImplemented = () => {
  if (getControlsNotImplementedPromise) {
    // A promise to getControls() is already in-flight.  Return it rather than
    // create a new promise
    return getControlsNotImplementedPromise;
  }

  getControlsNotImplementedPromise = new Promise(res => {
    setTimeout(() => {
      getControlsNotImplementedPromise = null;
      //console.log(Controls.filter(c => c.state && !c.state.isImplemented))
      return res(Controls.filter(c => c.state && !c.state.isImplemented))
    }, DELAY)
  })

  return getControlsNotImplementedPromise;
}

const getControlsUnknown = () => {
  if (getControlsUnknownPromise) {
    // A promise to getControls() is already in-flight.  Return it rather than
    // create a new promise
    return getControlsUnknownPromise;
  }

  getControlsUnknownPromise = new Promise(res => {
    setTimeout(() => {
      getControlsUnknownPromise = null;
      return res(Controls.filter(c => !c.state))
    }, DELAY)
  })

  return getControlsUnknownPromise;
}

export {
  getControlsAll, 
  getControlsImplemented,
  getControlsNotImplemented,
  getControlsUnknown
};
