import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {getControlsAll, getControlsImplemented, getControlsNotImplemented, getControlsUnknown} from './api';
import { withRouter, Redirect, Route, Switch} from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import {CssBaseline, Grid} from '@material-ui/core/';
import Theme from './theme';
import AppHeader from './components/AppHeader';
import ListFilters from './components/ListFilters'
import ControlList from './components/ControlList';
import ControlMain from './components/ControlMain';
import displayPercentage from './utils/DisplayPercentage';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faWindowClose, faCheckSquare, faQuestion} from '@fortawesome/free-solid-svg-icons'

library.add(faWindowClose, faCheckSquare, faQuestion)

const styles = {
  appRoot: {
    flexGrow: 1,
    fontSize: '1rem',
    padding: 12
  }
};

class App extends Component {
  constructor(){
    super()
    this.state = {
      controls: null,
      filters: null
    }
  }
  componentWillMount() {
    this.unlisten = this.props.history.listen( (location, action) => {} );
  }
  componentWillUnmount() {
      this.unlisten();
  }
  componentDidMount() {
    
        getControlsAll().then(controls => {
          this.createFilterData(controls);



         // this.updateSelected(controls)
        })

  }
  
  createFilterData(controls){
    
    let all = controls,
        yes = [],
        no = [],
        unknown = [];

    controls.forEach(control => {
      if(!control.state) {
        unknown.push(control)
      } else if( control.state.isImplemented ) {
        yes.push(control)
      } else {
        no.push(control)
      }
    })

    this.setState({
      filters: [
        {
          name: "all",
          total: all.length,
          controls: all,
          label: "All Controls",
          active: true,
          slug: "/all"
        },
        { 
          name: "implemented",
          total: yes.length,
          percentage: displayPercentage(yes.length, all.length, 0),
          controls: yes,
          label: "Implemented",
          slug: "/implemented"
        },
        {
          name: "not-implemented",
          total: no.length,
          percentage: displayPercentage(no.length, all.length, 0),
          controls: no,
          label: "Not Implemented",
          slug: "/not-implemented"
        },
        {
          name: "unknown",
          total: unknown.length,
          percentage: displayPercentage(unknown.length, all.length, 0),
          controls: unknown,
          label: "Unknown",
          slug: "/unknown"
        }
      ]
    }) 

  }
  updateSelectedFilter(filterId){
    const filters = this.state.filters.map( f => {
      ( f.name === filterId ) ? f.active = true : f.active = false;
      return f;
    })
    
    return filters;
  }
  updateSelected( controls, controlId = 0){
    
    controls.map( control => {
      ( control.id === parseInt(controlId) ) ? control.selected = true : control.selected = false;
      return control;
    })
    
    return controls;
  }

  getControl(filter, match){

    return filter.controls.find(c =>  c.id.toString() === match.params.controlId)

  }

  render() {
    const { controls, filters } = this.state;
    const { classes } = this.props; 
    return (
      
        <MuiThemeProvider theme={Theme}>
          <CssBaseline />
          <div className={classes.appRoot}>
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="flex-start"
              spacing={24}
            > 
              { filters && 
                <>
                  <Grid item xs={12} style={{padding:0}}>
                    <AppHeader />
                  </Grid>
                  <Grid item xs={12} style={{padding:0}}>
                   
                    <Route path='/controls/:filter' render={({ match }) => {
                      let filters = this.updateSelectedFilter(match.params.filter);
                      return ( <ListFilters filters={filters} /> )
                    }} />
                  </Grid> 
                  <Grid item md={6} style={{padding:'1px 0 0'}}>
                    <Route path='/controls/:filter/:id' render={({ match }) => {
                      let filter = filters.find(c => c.name === match.params.filter);
                  
                      let controls = this.updateSelected(filter.controls, match.params.id);
                        return (  <ControlList slug={filter.slug} controls={controls} style={{padding:0}} /> )
                    }} />
                  </Grid>
                  
                  <Grid item md={6} style={{padding: 15}}>
                  
                      <main>

                          <Route exact path='/' render={() => (
                            <Redirect to={{ pathname: `/controls/all/${filters[0].controls[0].id}` }} />
                          )} />

                          <Route exact path='/controls/:filter' render={({ match }) => {
                            let filter = filters.find(c => c.name === match.params.filter);
                            
                            return ( <Redirect to={{ pathname: `/controls/${match.params.filter}/${filter.controls[0].id}` }} /> )
                          }} />


                          <Route path='/controls/:filter/:controlId' render={({ match }) => {
                              let filter = filters.find(c => c.name === match.params.filter);
                             // this.updateSelected(filter.controls, match.params.controlId)
                              return ( <ControlMain control={this.getControl(filter, match)}/> )
                          }} />

                        </main>
                      
                  </Grid>
                </>
              }
            </Grid>
          </div>
        </MuiThemeProvider>
      
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(App))