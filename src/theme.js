import { createMuiTheme } from '@material-ui/core/styles';


export const Theme = createMuiTheme({
  breakpoints:{
    keys: {
      0: "xs",
      1: "sm",
      3: "md",
      4: "lg",
      5: "xl"
    },
    values: {
      xs: 0,
      sm: 600,
      md: 745,
      lg: 1280,
      xl: 1920
    }

  },
  palette: {
    text:{
      primary: '#7b91a9',
    }
  },
  shape: {
      borderRadius: 0,
  },
  typography: {
      useNextVariants: true,
      subtitle1: {
          fontSize: '0.85em'
      },
      h5: {
        letterSpacing: '0.025em',
        color: '#2e3847',
        fontWeight: 600,
        fontSize: '0.95em'
      },
      h6: {
        letterSpacing: '0.025em',
        color: 'inerit',
        fontWeight: 600,
        fontSize: '0.75em'
      },
      fontSize: 14,
  },
  custom: {
    orange: '#ffb01f',
    red: '#ed0505',
    green: '#2fcc70',
    blue: '#304ffe',
    lightBlue: '#e8f2ff',
    lightGray: '#f7faff',
    borderColor: '#f5f5f5'
  },
  mixins: {
    listTitle: {
      fontWeight: 'bold',fontSize: '0.7em',backgroundColor: '#f7faff', padding: '1.5em 1.35em'
    },
    panelHeader: ({
      fontSize: '1.35em',
      padding: '1.25em 15px 1em',
      fontWeight: 500,
    }),
    panelHeaderSub: {
      padding: '1.35em 15px',
      borderWidth: '1px 0',
      fontWeight: 600,
      whiteSpace:'nowrap',
      textTransform: 'uppercase',
      fontSize: '0.7em',
      letterSpacing: '0.02em',
    },
    panelBody: {
      padding: '1.25em 15px 1.75em',
      fontSize: '1em',
      fontWeight: 500,
      boxShadow: 'inset 0 2px 7px rgba(0,0,0, 0.1)',
      borderRadius: '0 0 5px 5px'
    },
  }

});

export default Theme;