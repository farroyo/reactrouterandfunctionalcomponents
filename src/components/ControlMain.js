import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-sticky-el';
import { withStyles } from '@material-ui/core/styles';
import {Paper, Typography} from '@material-ui/core';
import StatusDisplay from '../utils/DisplayStatusWithIconAndColor';

const styles = theme => ({
  paper:{
    padding: '0',
    borderRadius: 5,
  },
  sticky: {
    top: '60px!important',
  }

});


const ControlMain = ({ control, classes, theme }) => {
  //don't display anything if control is undefined
  if(!control) {
    return ('');
  }
  return (

    <Sticky stickyClassName={classes.sticky} topOffset={-60}>
      <Paper component="section" className={classes.paper}>
        <Typography component="div" variant="h4" style={{...theme.mixins.panelHeader}}>{control.name}</Typography>
        <Typography component="div" variant="subtitle1" style={{border: `1px solid ${theme.palette.grey[300]}`, ...theme.mixins.panelHeaderSub }}>Status: { <StatusDisplay control={control} /> }</Typography>
        <Typography component="div" variant="body1" paragraph style={{...theme.mixins.panelBody, backgroundColor: theme.custom.lightGray }}>{control.text}</Typography> 
      </Paper>
    </Sticky>

  )
}

ControlMain.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(ControlMain);
