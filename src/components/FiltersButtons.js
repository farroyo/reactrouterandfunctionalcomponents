import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import classNames from 'classnames';
import CreateStatusIcon from '../utils/CreateStatusIcon';
import {Button} from '@material-ui/core/';
import {withRouter, Link} from 'react-router-dom';

const styles = theme => ({
    btnClass: {
        fontWeight: 600,
        border: `1px solid ${theme.palette.grey[300]}`,
        borderRightWidth: 0,
        color: 'inherit',
        letterSpacing: '0.01em',
        fontSize: '0.65em',
        minHeight: 20,
        padding: '6px 8px',
        
        '&:first-of-type': {
          borderRadius: '3px 0 0 3px',
          marginLeft: 15,
        },
        '&:last-of-type': {
          borderRightWidth: 1,
          borderRadius: '0 3px 3px 0'
        },
        '&:hover': {
          backgroundColor: theme.custom.lightBlue,
          color: theme.custom.blue,
           '& $percentage': {
             color: 'inherit'
           } 
        },
    },
    active: {
        backgroundColor: theme.custom.lightGray,
        boxShadow: 'inset 0 2px 2px rgba(0,0,0, 0.1)'
    },
    percentage: {
        color: theme.palette.grey[400],
        display: 'inline-block',
        marginLeft: 10,
        fontWeight: 'normal',
    },
      
});

const FiltersButtons = ({filters, classes, updateList, history}) => (

    filters.map( (filter, index) => {

        return (
            
            <Button 
                key={`filter${index}`}
                className={classNames(classes.btnClass, filter.active && classes.active)} 
                component={Link}
                to={`/controls${filter.slug}`}
            >
                {index > 0 ? <CreateStatusIcon name={filter.name} /> : ''}
                {index === 0 ? `All ${filter.total} Controls` : `${filter.total} ${filter.label}`}
                {index > 0 ? <span className={classes.percentage}>{filter.percentage}%</span>: ''}
            </Button>
        )

    })
)
FiltersButtons.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withRouter( withStyles(styles)(FiltersButtons) )