import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {MenuItem, FormControl, Select} from '@material-ui/core/';

import CreateStatusIcon from '../utils/CreateStatusIcon';


const styles = theme => ({
    formControl: {
        marginLeft: 20,
        minWidth: 250,
        padding: 0,
    },
    selectEmpty: {
        marginTop: 0,
    },
    percentage: {
        color: theme.palette.grey[400],
        display: 'inline-block',
        marginLeft: 10,
        fontWeight: 'normal',
    },
});

const selectness = React.createRef();
const handleChange = event => {
  debugger
  alert('A name was submitted: ' + selectness.current.value);
  event.preventDefault();
};

const Options = ({classes, theme, filters}) => {
    
    let options = filters.map( (filter, index) => {
    
        return (
            <MenuItem key={`filter_option_${filter.name}`} value={filter.name}>
                { (index > 0 ) ? <CreateStatusIcon name={filter.name} /> : ''}
                {index === 0 ? `All ${filter.total} Controls` : `${filter.total} ${filter.label}`}
                {index > 0 ? <span className={classes.percentage}>{filter.percentage}%</span>: ''}
            </MenuItem>
        )
    
    });

    return ( options  );

}

const FiltersSelect = ({classes, theme, filters}) => {
    
    return (

        <FormControl className={classes.formControl}>               
            <Select
                value=""
                onChange={handleChange}
                displayEmpty
                name="age"
                className={classes.selectEmpty}
                autoWidth
                ref={selectness}
            >
                <Options classes={classes} theme={theme} filters={filters}  />
            </Select>
        </FormControl>

    )
}
FiltersSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};
export default withStyles(styles, {withTheme: true})(FiltersSelect)
