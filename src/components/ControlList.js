import React from 'react';
import PropTypes from 'prop-types';

import { Route, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import {Paper, Table, TableBody, TableCell, TableHead, TableRow, Typography} from '@material-ui/core';

import StatusDisplay from '../utils/DisplayStatusWithIconAndColor';
import Sticky from 'react-sticky-el';
import classNames from 'classnames';


const styles = theme => ({
  sticky: {
    top: '46px!important',
    zIndex: 1,
    borderTop: `1px solid ${theme.palette.grey[300]}`,
    boxShadow: `0px 5px 10px -5px ${theme.palette.grey[300]}`,
  },

  link: {
    textDecoration: 'none',
    ...theme.typography.captionNext,
    borderBottom: `1px solid ${theme.custom.borderColor}`,
    display: 'block',
    '&:hover':{
      backgroundColor: `${theme.custom.lightGray}`,
    },
    '&:last-of-type': {
      marginBottom: '1em',
    },
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.grey[200],
      '&:hover': {
        backgroundColor: `${theme.custom.lightGray}`,
      },
    },
    [theme.breakpoints.up("md")]: {
      '&:nth-of-type(odd)': {
        backgroundColor: 'initial',
      },
      
    }
  },
  selected:{
    backgroundColor: `${theme.custom.blue}`,
    color: theme.palette.common.white,
    '&:hover':{
      backgroundColor: `${theme.custom.blue}`,
      color: theme.palette.common.white,
    },
    '&:nth-of-type(odd)': {
      backgroundColor: `${theme.custom.blue}`,
      '&:hover':{
        backgroundColor: `${theme.custom.blue}`,
        color: theme.palette.common.white,
      }
    },
  },
  dl: {
    display: 'flex',
    alignItems: 'center',
    justifyContent:  'flex-start',
    padding: '0.5em 1em',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: 0,
    minHeight: 32,
    
    [theme.breakpoints.up('md')]: {
      padding: '0.15em 1em',
    },
  },
  dl_header: {
  
    margin: 0,
    minHeight: 32,
    padding: '0 0.75em',
    backgroundColor: theme.palette.common.white,
    borderBottom: `1px solid ${theme.custom.borderColor}`,
    display: 'none',
    '& $dl_dt': {
        display: 'initial',
      ...theme.typography.button,
    },
    
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    },
  },

  dl_dt: {
   
    margin: 0,
    padding:0,
    fontWeight: 'bold',
    flex: 'none',
    '&:nth-of-type(2)':{
      flex:' 1 1 100%',
    },
    
    [theme.breakpoints.up('md')]: {
      display: 'none',

      '&:nth-of-type(1)':{
        flex: '0 1 15%',
        minWidth: 75,
      },
      '&:nth-of-type(2)':{
        flex: '1',
      },
      '&:last-of-type': {
        marginLeft: 'auto',
        flex: 'none',
        textAlign: 'right',
        paddingRight: 0,
      },

    },

    
  },

  dl_dd: {
    margin:'0 0 0 0.5em',
    padding:0,
    flex: 1,
    
    '&:nth-of-type(2)':{
      flex:'0 1 100%',
      marginLeft: 0,
    },
    '&:last-of-type': {
      order: 1,
    },
    [theme.breakpoints.up('md')]: {
      paddingRight: 20,
      marginLeft: 0,
      '&:nth-of-type(1)':{
        flex: '0 1 15%',
        minWidth: 75,
      },
      '&:nth-of-type(2)':{
        flex: '1',
      },
      '&:last-of-type': {
        marginLeft: 'auto',
        flex: '0 1 20%',
        textAlign: 'right',
        paddingRight: 0,
      },
    },
  }
  
});
/* jshint ignore:start */
const ControlList = ({ controls, classes, theme, updateSelected, slug }) => (
  <>
  <Paper>

    <Typography color="textSecondary" component="h6" variant="button" style={{ ...theme.mixins.listTitle }}>Controls</Typography>
    <Sticky stickyClassName={classes.sticky} topOffset={-60}>
      <dl className={classNames(classes.dl, classes.dl_header)}>
        <dt className={classes.dl_dt}>Control</dt>
        <dt className={classes.dl_dt}>Description</dt>  
        <dt className={classes.dl_dt}>Status</dt>
      </dl>
    </Sticky>
    
      {
        controls.map(control => (
          
          <Link 
            to={`/controls${slug}/${control.id}`} 
            className={classNames(classes.link, control.selected && classes.selected)}
            key={`/controls${slug}/${control.id}`}
            data-key={`/controls${slug}/${control.id}`}
          
          >

            <dl className={classes.dl}>
              <dt className={classes.dl_dt}>Control:</dt>
              <dd className={classes.dl_dd}>{control.name}</dd>
              <dt className={classes.dl_dt}>Description:</dt>
              <dd className={classes.dl_dd}>{control.text}</dd>
              <dt className={classes.dl_dt}>Status</dt>
              <dd className={classes.dl_dd}>{ <StatusDisplay control={control} /> }</dd>
            </dl>
          </Link>
        ))
      }
  </Paper>
  </>
)
/* jshint ignore:end */
ControlList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles, {withTheme: true})(ControlList);