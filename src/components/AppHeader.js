import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  AppBar: {
    minHeight: 0,
    boxShadow: 'none',
    backgroundColor: theme.palette.common.white,
    padding: '0.75em 1em 0.5em',
    whiteSpace: 'nowrap'
  }
});

function SimpleAppBar(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default" className={classes.AppBar}>
        <Typography variant="h5">
          Implementation Assessment
        </Typography>
        <Typography variant="subtitle1">
          Evaluate the implementation status of your security controls.
        </Typography>
      </AppBar>
    </div>
  );
}

SimpleAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleAppBar);