import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {AppBar, Toolbar, Typography, Grid} from '@material-ui/core/';
import Sticky from 'react-sticky-el';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import FiltersButtons from './FiltersButtons';
import FiltersSelect from './FiltersSelect';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  sticky: {
    zIndex: 1,
  },
  AppBar: {
    minHeight: 0,
    backgroundColor: theme.palette.common.white,
    padding: 0,
    boxShadow: '0 1px 3px rgba(0,0,0, 0.1)',
    borderTop: `1px solid ${theme.custom.borderColor}`
  },
  toolbarWrapper: {
    color: theme.palette.text.primary,
    minHeight: 0,
    padding: '0.5em 1em'
  },
});

const ListFilters = ({ classes, theme, filters, updateList, history, width }) => {
  
  return (
    <div className={classes.root}>
      <Sticky stickyClassName={classes.sticky}>
        <AppBar position="static" color="default" className={classes.AppBar}>
          <Toolbar className={classes.toolbarWrapper}>
          <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
            >
              <Typography variant="h6">
                Filter Controls
              </Typography>

              { 
                isWidthUp('md', width) ? 
                <FiltersButtons updateList={updateList} filters={filters} /> :
                <FiltersSelect updateList={updateList} filters={filters} /> 
              }

            </Grid>
          </Toolbar>
        </AppBar>
      </Sticky>
    </div>
  );
}

ListFilters.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withWidth()( withStyles(styles, {withTheme: true})(ListFilters) )