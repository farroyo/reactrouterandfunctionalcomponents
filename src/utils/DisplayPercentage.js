const DisplayPercentage = (num1=0, num2=0, fixedCount=0) => parseFloat(num1/num2 * 100).toFixed(fixedCount)
export default DisplayPercentage;

