import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const styles = theme => ({
    btnIcon: {
        marginRight: theme.spacing.unit,
        fontSize: '1em',
        marginLeft: 3,
        textTransform: 'uppercase',
        transform: 'scale(1.6)'
    },
    active: {},
    qIconWrapper: {
        height:'1.5em', 
        width:'1.5em',
        position:'relative', 
        backgroundColor: theme.custom.orange, 
        borderRadius: 3, 
        marginRight: 7,
        marginLeft: 0
      },
    qIcon: {
        fontSize:'1em', 
        position:'absolute', 
        top: '45%', 
        left: '50%', 
        transform:'translate(-50%,-50%)', 
        color: `white`, 
        marginLeft: 0
    }
});

const CreateIcon = ({name, classes, theme}) => {
  
    let icon = null;
  
    switch(name){
      case 'implemented':
       icon = <FontAwesomeIcon icon="check-square" style={{color: `${theme.custom.green}`}} className={classes.btnIcon} />
        break;
      case 'not-implemented':
       icon = <FontAwesomeIcon icon="window-close" style={{color: `${theme.custom.red}`}} className={classes.btnIcon} />
        break;
      default:
       icon = <span className={classes.qIconWrapper}><FontAwesomeIcon icon="question" className={classes.qIcon} /></span>
    }
    return (icon)
}
CreateIcon.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles, {withTheme: true} )(CreateIcon);
