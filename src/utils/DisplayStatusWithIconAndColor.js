
/* A utility that will render the proper status, 
icon and color given a {control object} */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//applied styles
const styles = theme => ({
    StatusCellStyle: {
        textTransform:'uppercase',
        whiteSpace:'nowrap',
        display: 'inline-flex',
        alignItems: 'center',
        fontSize: 'inherit',
        fontWeight: 'bold',
        letterSpacing: '0.05em'
    },

    statusIcon: {
        fontSize: '1.4em',
        marginLeft: 5
    },
    qIconWrapper: {
        height:15, 
        width:15,
        position:'relative', 
        backgroundColor: theme.custom.orange, 
        borderRadius: 3,
        marginLeft: 5
    },
    qIcon: {
        fontSize:10, 
        position:'absolute', 
        top: '50%', 
        left: '50%', 
        transform:'translate(-50%,-50%)', 
        color: `white`, 
        marginLeft: 0
    }
})

//receives 3 props
// applicable control data
// classes and theme props are returned from withStyles

const StatusDisplay = ({ control, classes, theme }) => {
    
    let status = "Unknown",
        color = theme.custom.orange;
  
    if(typeof control.state !== "undefined"){
      status = `${ !control.state.isImplemented ? 'Not ' : ''}Implemented`;
    }
  
    let icon = null;
  
    switch( status.replace(/\s/g, '') ){
      case 'Implemented':
       icon = <FontAwesomeIcon icon="check-square" className={classes.statusIcon} />
       color = theme.custom.green
        break;
      case 'NotImplemented':
       icon = <FontAwesomeIcon icon="window-close" className={classes.statusIcon} />
       color = theme.custom.red
        break;
      default:
       icon = <span className={classes.qIconWrapper}><FontAwesomeIcon icon="question" className={classes.qIcon} /></span>
    }
    return (
        <span style={{color: color}} className={classes.StatusCellStyle}  ><span>{status}</span> {icon}</span>
    );
}


StatusDisplay.propTypes = {
    classes: PropTypes.object.isRequired
};
  
export default withStyles(styles, {withTheme: true})(StatusDisplay);